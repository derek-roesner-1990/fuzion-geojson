var extend = require('node.extend');

var commonConfig = {
} ;

var developmentConfig = {
  db: 'mongodb://x.x.x.x/fuzion_db',
  pg_db: 'postgres://fuzion:xx@localhost:5433/fuzion_db',
  //pg_db: 'postgres://fuzion:xxx@1x.x.x.x/fuzion_db',
  cors_urls: ['http://localhost:3000', 'http://127.0.0.1:4051', 'http://127.0.0.1:4051/va/warnings']
};

var productionConfig = {
  db: 'mongodb://x.x.0.3,x.x.0.4,x.x.0.5,/fuzion_db?replicaSet=rs0',
  pg_db: 'postgres://fuzion:xx@x.x.x.x/fuzion_db',
  cors_urls: ['https://wx2.weathertelematics.com', 'https://events.weathertelematics.com', 'https://alerts.weathertelematics.com', 'https://roadwx.weathertelematics.com']
} ;

module.exports = {
  development: extend(true, developmentConfig, commonConfig),
  production: extend(true, productionConfig, commonConfig)
} ;
