/******************************************************************************
                               Config Setup
******************************************************************************/

var env = process.env.NODE_ENV || 'development' ;
var config = require('./config.js')[env] ;

/******************************************************************************
                                App Setup
******************************************************************************/

var express = require('express');
var app = express();

// Server public assets (used to renew SSL right now)
app.use(express.static('public'));

/******************************************************************************
                          Body Parser Setup
******************************************************************************/

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/******************************************************************************
                     Mongo Database Connection
******************************************************************************/

// Mongoose import
var mongoose = require('mongoose');

// Mongoose connection to MongoDB
mongoose.connect(config.db, function (error) {
  if (error) {
    console.log(error);
  }
});

/******************************************************************************
                                 Routing
******************************************************************************/

var router = require('./routers/router') ;
app.use(router) ;

/******************************************************************************
                                  Server
******************************************************************************/

var port = process.env.PORT || 4051;

var server = app.listen(port, function () {

  var host = '127.0.0.1'; //server.address().address;
  var port = server.address().port;

  console.log('Fuzion geojson listening at http://%s:%s', host, port);

});

module.exports = server ;

/******************************************************************************
                              Socket IO
******************************************************************************/

var io = require('socket.io').listen(server);

io.sockets.on('connection', function (socket) {
  console.log('client connect');

  socket.on('lightning-flash-src', function(data) {
    io.emit('lightning-flash-client', data);
  });

  socket.on('enable-rwc-layer', function(data) {
    var messageName = 'disable-rwc-layers-' + data.guid;
    io.emit(messageName, data.layerName);
  });

  socket.on('contours_trigger', function(data) {
    console.log('firing contours updates');
    io.emit('update-contours', data);
  });

  socket.on('hrrr_trigger', function (data) {
    console.log('firing hrrr contours updates');
    io.emit('update-hrrr-contours', data);
  });

  socket.on('assessed_roads_trigger', function (data) {
    console.log('firing assessed roads updates', data);
    io.emit('update-assessed-roads', data);
  });

  socket.on('disconnect', function() {
    console.log('client disconnect');
    socket.disconnect();
  });
});

module.exports = server ;