var chai = require('chai') ;
var chaiHttp = require('chai-http') ;
var server = require.main.require('server.js') ;
var should = chai.should() ;

chai.use(chaiHttp) ; // NOTE: HTTP tests are async. Use done, or promises.

var acceptableTimeout = 1000 * 30 ;

var itShouldSucceedOnGetAt = function(path) {
  it('should return 200 on GET', function(done) {
    chai.request(server)
    .get(path)
    .end(function(err, res) {
      res.should.have.status(200) ;
      done() ;
    }) ;
  }) ;
} ;

describe('fuzion-geojson', function() {
  describe('router', function() {
    this.timeout(acceptableTimeout) ;
    describe('/', function() {
      itShouldSucceedOnGetAt('/') ;

      describe('/asos', function() {
        itShouldSucceedOnGetAt('/asos') ;

        describe('/temp', function() {
          itShouldSucceedOnGetAt('/asos/temp') ;
        }) ;
      }) ;

      describe('/rwis', function() {
        itShouldSucceedOnGetAt('/rwis');

        describe('/temp', function() {
          itShouldSucceedOnGetAt('/rwis/temp') ;
        }) ;

        describe('/road_temp', function() {
          itShouldSucceedOnGetAt('/rwis/road_temp') ;
        }) ;

        describe('/road_state', function() {
          itShouldSucceedOnGetAt('/rwis/road_state') ;
        }) ;

      }) ;

      describe('/wtx', function() {
        itShouldSucceedOnGetAt('/wtx') ;

        describe('/road_temp', function() {
          itShouldSucceedOnGetAt('/wtx/road_temp') ;
        }) ;

        describe('/temp', function() {
          itShouldSucceedOnGetAt('/wtx/temp') ;
        }) ;
      }) ;
    }) ;
  }) ;
}) ;
