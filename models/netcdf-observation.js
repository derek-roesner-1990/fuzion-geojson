var mongoose = require('mongoose') ;
var sharedSchema = require('../schemas/shared-schema') ;

module.exports = mongoose.model("NetCDFObservation", sharedSchema);
