var mongoose = require('mongoose') ;
var vaLayer = require('../schemas/vehicle-alert-layer-schema') ;

module.exports = mongoose.model("LiveVALayer", vaLayer);
