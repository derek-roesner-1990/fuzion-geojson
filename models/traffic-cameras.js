var mongoose = require('mongoose') ;
var trafficCameraSchema = require('../schemas/traffic-camera-schema') ;

//module.exports = mongoose.model("TrafficCamera", trafficCameraSchema);
module.exports = mongoose.model("LiveCamera", trafficCameraSchema);
