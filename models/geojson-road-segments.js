var mongoose = require('mongoose') ;
var roadSegmentSchema = require('../schemas/road-segment-schema') ;

module.exports = mongoose.model("GeoJSONRoadSegment", roadSegmentSchema);
