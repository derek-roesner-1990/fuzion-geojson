var mongoose = require('mongoose') ;
var qcSchema = require('../schemas/qc-schema') ;

module.exports = mongoose.model("QualityScore", qcSchema);
