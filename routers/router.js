var express = require('express') ;
var app = express.Router() ;

var env = process.env.NODE_ENV || 'development' ;
var config = require('../config.js')[env] ;

// Setup CORS
app.use(function (req, res, next) {
  // Website you wish to allow to connect

  var allowedOrigins = config.cors_urls;
  var origin = req.headers.origin;
  if(allowedOrigins.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  
  // Request methods you wish to allow
  //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  // res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

/******************************************************************************
                                Routes
******************************************************************************/

app.get('/', function (req, res) {
  res.send("<a href='/rwis/temp'>Show RWIS Temperature Data</a>");
});

// Waiting for data to get into PG
//app.use('/asos', require('./asos')) ;

// Waiting for data to get into PG
//app.use('/rwis', require('./rwis')) ;

// Waiting for data to get into PG
//app.use('/wtx', require('./wtx')) ;

app.use('/nws', require('./nws')) ;

app.use('/va', require('./va')) ;

app.use('/lightning', require('./lightning')) ;

app.use('/impedance', require('./impedance')) ;

app.use('/legend', require('./legend')) ;

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
