var fuzionUtilities = require('../lib/utilities');

var express = require('express') ;
var request = require('request');
var compress = require('compression');
var app = express.Router() ;
app.use(compress());
var math = require('mathjs');

/******************************************************************************
                               Config Setup
******************************************************************************/

var env = process.env.NODE_ENV || 'development' ;
var config = require('../config.js')[env] ;

/******************************************************************************
                              DB Setup
******************************************************************************/

var pg = require('pg');
var connectionString = config.pg_db;

/******************************************************************************
                                Routes
******************************************************************************/

// TODO: this should be refactored to persist the legends

var legends = {
  road_cond: {
    id: 'road_cond',
    title: 'Condition Risks',
    range_title: '',
    ranges: [],
    range_colours: [],
    finalText: '',
    state_title: '',
    states: ['Wet', 'Snowy', 'Icy'],
    state_colours: ['#8BBA71', '#0F78A5', '#A10A40']
  },
  qpe: {
    id: 'qpe',
    title: 'Hydroplaning Risk (mm)',
    range_title: '',
    ranges: [2, 4, 6],
    range_colours: ['#E09485', '#FF5533', '#CC2200'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  ice: {
    id: 'ice',
    title: 'Icing Risk (%)',
    range_title: '',
    ranges: [1, 33, 66],
    range_colours: ['#BCAAA4', '#795548', '#4E342E'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  lit: {
    id: 'lit',
    title: 'Lightning Risk (%)',
    range_title: '',
    ranges: [1, 33, 66],
    range_colours: ['#BF7FBF', '#990099', '#400040'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  strikes: {
    id: 'strikes',
    title: 'Lightning Strikes',
    range_title: 'Cloud to Ground (min)',
    ranges: [0, 5, 10, 15, 20, 25],
    range_colours: ['#e0e1dd', '#e0e110', '#e09d10', '#e05910', '#e01410', '#ac1410'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  hail: {
    id: 'hail',
    title: 'Hail Risk (%)',
    range_title: '',
    ranges: [1, 33, 66],
    range_colours: ['#FFFF99', '#FFFF00', '#595904'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  refl: {
    id: 'refl',
    title: 'Reflectivity (dBZ)',
    range_title: '',
    ranges: [-5, 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70],
    range_colours: ['#6f6a32', '#00eced', '#009ef9', '#0300cc', '#02fd02', '#01c501', '#008e00', '#fdf802', '#e5bc00', '#ff2700', '#fd0000', '#d40000', '#bc0000', '#f800fd', '#9854c6', '#fdfdfd'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  road_temp: {
    id: 'road_temp',
    title: 'Road Temperature (°F)',
    range_title: '',
    ranges: [-100, 32, 73],
    range_colours: ['#A3C7C5', '#A38F5A', '#AD280E'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  vis: {
    id: 'vis',
    title: 'Visibility Risk (m)',
    range_title: '',
    ranges: [3000, 1000, 500],
    range_colours: ['#ADE0EB', '#3DD6F5', '#004C99'],
    finalText: '-',
    state_title: '',
    states: [],
    state_colours: []
  },
  wind: {
    id: 'wind',
    title: 'Windspeed Risk (m/s)',
    range_title: '',
    ranges: [10, 15, 20],
    range_colours: ['#85E085', '#089208', '#044904'],
    finalText: '+',
    state_title: '',
    states: [],
    state_colours: []
  },
  delay : {
    id: 'wc0',
    title: 'Delay Risk',
    range_title: '',
    ranges: [],
    range_colours: [],
    finalText: '',
    state_title: '',
    states: ['93%', '81%', '65%'],
    state_colours: ['#8BBA71', '#0F78A5', '#A10A40']
  }
}

app.get('/', function (req, res) {
  res.json(legends);
});

app.get('/:legend_id', function (req, res) {
  var legend_id = req.params.legend_id

  if (legend_id && legend_id in legends) {
    res.json(legends[req.params.legend_id]);
  } else {
    res.status(404).json({
      error: `${legend_id} not found, check root legend path for available legends`
    })
  }

});

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
