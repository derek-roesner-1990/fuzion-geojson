var express = require('express') ;
var compress = require('compression');
var app = express.Router() ;
app.use(compress());

var fuzionUtilities = require('../lib/utilities');
var topojson = require("topojson");

/******************************************************************************
                               Config Setup
******************************************************************************/

var env = process.env.NODE_ENV || 'development' ;
var config = require('../config.js')[env] ;

/******************************************************************************
                              DB Setup
******************************************************************************/

var pg = require('pg');
var connectionString = config.pg_db;

/******************************************************************************
                                Routes
******************************************************************************/

app.get('/', function (req, res) {
  res.send("<p>Impedance Data</p>");
});

app.get('/condition\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
      'Feature' as type,
      ST_AsGeoJSON(geom)::json as geometry,
      row_to_json((
        SELECT d FROM (
          SELECT
            road_condition,
            speed_factor,
            forecast_time,
            valid_time
        ) d
      )) as properties
      FROM impedance_polygons
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
