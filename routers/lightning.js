var express = require('express') ;
var app = express.Router() ;

var fuzionUtilities = require('../lib/utilities');
var topojson = require("topojson");

/******************************************************************************
                               Config Setup
******************************************************************************/

var env = process.env.NODE_ENV || 'development' ;
var config = require('../config.js')[env] ;

/******************************************************************************
                              DB Setup
******************************************************************************/

var pg = require('pg');
var connectionString = config.pg_db;

/******************************************************************************
                                Routes
******************************************************************************/

app.get('/', function (req, res) {
  res.send("<p>Lightning Data</p>");
});

app.get('/flash', function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
    SELECT
      ST_AsGeoJSON(geom)::json as geometry,
      row_to_json((SELECT d FROM (SELECT id, polarity, peak_current, "timestamp", height) d)) as properties
    FROM lightning_pulses
    WHERE cgmultiply > 0
    AND timestamp > ` + (Math.round(new Date().getTime()/1000.0) - 1800) + ` ;`);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);
      var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});

      return res.json(topology);

    });
  });
});

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
