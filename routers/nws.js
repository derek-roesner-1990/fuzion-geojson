var request = require('request');
var through = require('through');
var express = require('express') ;
var app = express.Router() ;

/******************************************************************************
                            Schema and Model Setup
******************************************************************************/

var geojson = require('weather-alerts-geojson');
var parser = require('weather-alerts-parser');

/******************************************************************************
                                Routes
******************************************************************************/

app.get('/alerts', function (req, res) {

  var features = [];

  var fc = through(function write(data) {
      features.push(data);
      //this.queue(data);
    },
    function end () { //optional 
       console.log(features.length);
       var featureCollection = {
         type: "FeatureCollection",
         features: features
       }
       res.send(JSON.stringify(featureCollection))

    })

  var r = request.get('http://alerts.weather.gov/cap/us.php?x=0')
    .pipe(parser.stream())
    .pipe(geojson.stream({'stylize': true}))
    //.pipe(geojson.collect({'sort': false, 'flatten': false}))
    .pipe(fc)

});

module.exports = app ;