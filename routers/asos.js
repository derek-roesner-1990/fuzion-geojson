var fuzionUtilities = require('../lib/utilities');

var express = require('express') ;
var app = express.Router() ;
var math = require('mathjs');

/******************************************************************************
                               Config Setup
******************************************************************************/

var env = process.env.NODE_ENV || 'development' ;
var config = require('../config.js')[env] ;

/******************************************************************************
                              DB Setup
******************************************************************************/

var pg = require('pg');
var connectionString = config.pg_db;

/******************************************************************************
                                Routes
******************************************************************************/

app.get('/', function (req, res) {
  res.send("<p>ASOS QC Data Here</p>");
});

app.get('/temp', function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
    select distinct on (stationName)
      'Feature' as type,
      ST_AsGeoJSON(geom)::json as geometry,
      row_to_json((
        SELECT d FROM (
          SELECT
            stationName,
            time,
            temperature,
            longitude,
            latitude
        ) d
      )) as properties
    from asos_observations
    order by stationName, time DESC;
    `);

    query.on('row', function(row) {
      row.properties.colour = fuzionUtilities.colourFromTemp(row.properties.temperature)
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);
      //var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});

      return res.json(geoJSON);
    });
  });

});

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
