var fuzionUtilities = require('../lib/utilities');

var express = require('express') ;
var app = express.Router() ;

/******************************************************************************
                            Schema and Model Setup
******************************************************************************/


var NetCDFObs = require('../models/netcdf-observation') ;
var WTXObs = require('../models/wtx-observation') ;


/******************************************************************************
                                Routes
******************************************************************************/

app.get('/', function (req, res) {
  res.send("<p>RWIS QC Data Here</p>");
});

app.get('/temp', function (req, res) {

  var results = [];

  var distinctTrucks = WTXObs.find().distinct('metadata.deviceNumber', function(err, vehicleNumbers) {
    if(err)
      console.log(err) ;
    var promises = vehicleNumbers.map(function(vehicleNumber) {
      return new Promise(function(resolve, reject) {
        var agg = WTXObs.aggregate([
          { $match: { 'metadata.dataSource':'wtx', 'metadata.deviceNumber': vehicleNumber } },
          { $sort: { time: -1 } },
          { $limit: 1 },
          { $project:
            {
              _id: false,
              type: { $literal: "Feature" },
              geometry: {
                type: { $literal: "Point" },
                coordinates: "$position.coordinates"
              },
              properties: {
                position: "$position",
                time: "$time",
                observation: "$observation",
                metadata: "$metadata"
              }
            }
          }
        ]);
        agg.exec(function (err, docs) {
          if(err)
            return reject(err);
          docs = fuzionUtilities.addTempColour(docs);
          results.push(docs[0]);
          resolve();
        });
      });
    });

    Promise.all(promises)
    .then(function() {
      console.log("WTX Temperature Observations:" + results.length) ;
      var geoJSON = fuzionUtilities.createGeoJSON(results);
      res.json(geoJSON);
    })
    .catch(console.error);
  });
});

app.get('/road_temp', function (req, res) {

  var results = [];

  var distinctTrucks = WTXObs.find().distinct('metadata.deviceNumber', function(err, vehicleNumbers) {
    if(err)
      console.log(err) ;
    var promises = vehicleNumbers.map(function(vehicleNumber) {
      return new Promise(function(resolve, reject) {
        var agg = WTXObs.aggregate([
          { $match: { 'metadata.dataSource':'wtx', 'metadata.deviceNumber': vehicleNumber } },
          { $sort: { time: -1 } },
          { $limit: 40 },
          {
            $group:
              {
                _id: "$metadata.deviceNumber",
                line: { $push:  "$position.coordinates" },
                position: { $first: "$position" },
                time: { $first: "$time" },
                observation: { $first: "$observation" },
                road_avgs: {
                  $avg: { $cond: { if: { $ne : [ { $arrayElemAt: ["$observation.road.temperatures", 0] }, NaN] }, then: { $arrayElemAt: ["$observation.road.temperatures", 0] }, else: "NaN" } }
                },
                metadata: { $first: "$metadata" }
              }
          },
          { $project:
            {
              _id: false,
              type: { $literal: "Feature" },
              geometry: {
                type: { $literal: "LineString" },
                coordinates: "$line"
              },
              properties: {
                position: "$position",
                time: "$time",
                roadTemperature: "$road_avgs",
                metadata: "$metadata"
              }
            }
          }
        ]);
        agg.exec(function (err, docs) {
          if(err)
            return reject(err);
          docs = fuzionUtilities.addRoadTempColour(docs);
          results = results.concat(docs);
          resolve();
        });
      });
    });

    Promise.all(promises)
    .then(function() {
      console.log("WTX Road Temperature Observations:" + results.length);
      var geoJSON = fuzionUtilities.createGeoJSON(results);
      res.json(geoJSON);
    })
    .catch(console.error);
  });
});

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
