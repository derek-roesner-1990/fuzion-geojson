var fuzionUtilities = require('../lib/utilities');

var express = require('express') ;
var request = require('request');
var compress = require('compression');
var app = express.Router() ;
app.use(compress());
var math = require('mathjs');
var topojson = require("topojson");

/******************************************************************************
                            Schema and Model Setup
******************************************************************************/

var VAVehicles = require('../models/vehicle-alert-vehicles') ;
var TrafficCameras = require('../models/traffic-cameras');

/******************************************************************************
                               Config Setup
******************************************************************************/

var env = process.env.NODE_ENV || 'development' ;
var config = require('../config.js')[env] ;

/******************************************************************************
                              DB Setup
******************************************************************************/

var pg = require('pg');
var connectionString = config.pg_db;


/*****************
Utility functions
*****************/

function augment_json(json) {console.log(json)}; // Ignore that for now

/******************************************************************************
                                Routes
******************************************************************************/

app.get('/', function (req, res) {
  res.send("<p>Vehicle Alert Data Here</p>");
});


//app.get('/lat_lon_queries/:arguments?', fuzionUtilities.defaultParamMiddleware, function(req,res) {
app.get('/lat_lon_queries/product/:p/lat/:la/lon/:lo/time/:t', fuzionUtilities.defaultParamMiddleware, function(req,res) {
  var results = [];
  url = 'http://10.128.0.9:5354/latlontime';
  if (req.params.t == 'now'){
    full_url = url+'/'+req.params.p+'/'+req.params.la+'/'+req.params.lo
  } else {
    full_url = url+'/'+req.params.p+'/'+req.params.la+'/'+req.params.lo+'/'+req.params.t
  }
  console.log(full_url);
  request(full_url, { json: true }, (err2, res2, body) => {
    if (err2) { return console.log(err2); }
    return res.json(body);
  });
});



app.get('/hydroplaning\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];
  

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'qpe'
      ORDER BY risk_level
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

/********* BOUNDED BOX ROUTE **************/
//ST_MakeEnvelope(minLon(left), minLat(bottom), maxLon(right), maxLat(top), 4326)
app.get('/hydroplaning/bbox/minLon/:left/minLat/:bottom/maxLon/:right/maxLat/:top/\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    if (isNaN(Number(req.params.left)) || isNaN(Number(req.params.bottom)) || isNaN(Number(req.params.right)) || isNaN(Number(req.params.top))) {
      done();
      console.log("Incorrect input: only float values are acceptable.");
      return res.status(400).json({ success: false, data: "Incorrect input: only float values are acceptable."});
    }

    var left = parseFloat(req.params.left);
    var bottom = parseFloat(req.params.bottom);
    var right = parseFloat(req.params.right);
    var top = parseFloat(req.params.top);

    if(left >= right) {
      done();
      console.log("Incorrect input: western longitude value should be less than eastern longitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: western longitude value should be less than eastern longitude value."});
    }

    if(bottom >= top) {
      done();
      console.log("Incorrect input: southern latitude value should be less than northern latitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: southern latitude value should be less than northern latitude value."});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'qpe' AND geom && 
      ST_MakeEnvelope($1, $2, $3, $4, 4326)
      ORDER BY risk_level
    `,[left, bottom, right, top]);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/worldwide_hydroplaning\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
        SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index,
              ARRAY[minimum_value, maximum_value] as physical_values,
              variable_name as condition_type
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'qpe'
      ORDER BY risk_index
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

app.get('/visibility\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'visibility'
      ORDER BY risk_level
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

/********* BOUNDED BOX ROUTE **************/
app.get('/visibility/bbox/minLon/:left/minLat/:bottom/maxLon/:right/maxLat/:top/\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    if (isNaN(Number(req.params.left)) || isNaN(Number(req.params.bottom)) || isNaN(Number(req.params.right)) || isNaN(Number(req.params.top))) {
      done();
      console.log("Incorrect input: only float values are acceptable.");
      return res.status(400).json({ success: false, data: "Incorrect input: only float values are acceptable."});
    }

    var left = parseFloat(req.params.left);
    var bottom = parseFloat(req.params.bottom);
    var right = parseFloat(req.params.right);
    var top = parseFloat(req.params.top);

    if(left >= right) {
      done();
      console.log("Incorrect input: western longitude value should be less than eastern longitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: western longitude value should be less than eastern longitude value."});
    }

    if(bottom >= top) {
      done();
      console.log("Incorrect input: southern latitude value should be less than northern latitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: southern latitude value should be less than northern latitude value."});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'visibility' AND geom && 
      ST_MakeEnvelope($1, $2, $3, $4, 4326)
      ORDER BY risk_level
    `,[left, bottom, right, top]);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/sxm_visibility\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index,
              ARRAY[minimum_value, maximum_value] as physical_values,
              variable_name as condition_type
          ) d
        )) as properties
      FROM rap_contours
      WHERE variable_name = 'visibility'
      ORDER BY risk_index
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/worldwide_visibility\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index,
              ARRAY[minimum_value, maximum_value] as physical_values,
              variable_name as condition_type
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'visibility'
      ORDER BY risk_index
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});



app.get('/sxm_wind\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index,
              ARRAY[minimum_value, maximum_value] as physical_values,
              variable_name as condition_type
          ) d
        )) as properties
      FROM rap_contours
      WHERE variable_name = 'wind'
      ORDER BY risk_index
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/worldwide_wind\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index,
              ARRAY[minimum_value, maximum_value] as physical_values,
              variable_name as condition_type
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'windspeed'
      ORDER BY risk_index
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

app.get('/risk_index\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index as riskIndex,
              'None' as physical_values,
              condition_type
          ) d
        )) as properties
      FROM gfs_road_conditions WHERE forecast_time = '0'
      
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

/********* BOUNDED BOX ROUTE **************/
app.get('/risk_index/bbox/minLon/:left/minLat/:bottom/maxLon/:right/maxLat/:top/\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    if (isNaN(Number(req.params.left)) || isNaN(Number(req.params.bottom)) || isNaN(Number(req.params.right)) || isNaN(Number(req.params.top))) {
      done();
      console.log("Incorrect input: only float values are acceptable.");
      return res.status(400).json({ success: false, data: "Incorrect input: only float values are acceptable."});
    }

    var left = parseFloat(req.params.left);
    var bottom = parseFloat(req.params.bottom);
    var right = parseFloat(req.params.right);
    var top = parseFloat(req.params.top);

    if(left >= right) {
      done();
      console.log("Incorrect input: western longitude value should be less than eastern longitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: western longitude value should be less than eastern longitude value."});
    }

    if(bottom >= top) {
      done();
      console.log("Incorrect input: southern latitude value should be less than northern latitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: southern latitude value should be less than northern latitude value."});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index as riskIndex,
              'None' as physical_values,
              condition_type
          ) d
        )) as properties
      FROM gfs_road_conditions WHERE forecast_time = '0' AND geom && 
      ST_MakeEnvelope($1, $2, $3, $4, 4326)
    `,[left, bottom, right, top]);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/sxm_road_conditions\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index,
              'None' as physical_values,
              condition_type
          ) d
        )) as properties
      FROM rap_road_conditions
      
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/worldwide_road_conditions\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              valid_time,
              speed_factor,
              risk_index,
              'None' as physical_values,
              condition_type
          ) d
        )) as properties
      FROM gfs_road_conditions
      
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

app.get('/road_conditions\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              zone_type as roadCondition
          ) d
        )) as properties
      FROM gfs_road_conditions where forecast_time = '0'
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

/********* BOUNDED BOX ROUTE **************/
app.get('/road_conditions/bbox/minLon/:left/minLat/:bottom/maxLon/:right/maxLat/:top/\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    if (isNaN(Number(req.params.left)) || isNaN(Number(req.params.bottom)) || isNaN(Number(req.params.right)) || isNaN(Number(req.params.top))) {
      done();
      console.log("Incorrect input: only float values are acceptable.");
      return res.status(400).json({ success: false, data: "Incorrect input: only float values are acceptable."});
    }

    var left = parseFloat(req.params.left);
    var bottom = parseFloat(req.params.bottom);
    var right = parseFloat(req.params.right);
    var top = parseFloat(req.params.top);

    if(left >= right) {
      done();
      console.log("Incorrect input: western longitude value should be less than eastern longitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: western longitude value should be less than eastern longitude value."});
    }

    if(bottom >= top) {
      done();
      console.log("Incorrect input: southern latitude value should be less than northern latitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: southern latitude value should be less than northern latitude value."});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              zone_type as roadCondition
          ) d
        )) as properties
      FROM gfs_road_conditions where forecast_time = '0' AND geom && 
      ST_MakeEnvelope($1, $2, $3, $4, 4326)
    `,[left, bottom, right, top]);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/road_conditions_forecast/:forecast_time\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var forecast_time_int = parseInt(req.params.forecast_time)

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              zone_type as roadCondition
          ) d
        )) as properties
      FROM gfs_road_conditions
      WHERE forecast_time = ($1)
    `,[forecast_time_int]);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

/********* BOUNDED BOX ROUTE **************/
app.get('/road_conditions_forecast/bbox/:forecast_time/minLon/:left/minLat/:bottom/maxLon/:right/maxLat/:top/\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var forecast_time_int = parseInt(req.params.forecast_time);

    if (isNaN(Number(req.params.left)) || isNaN(Number(req.params.bottom)) || isNaN(Number(req.params.right)) || isNaN(Number(req.params.top)) || isNaN(Number(req.params.forecast_time)) ) {
      done();
      console.log("Incorrect input: only float values are acceptable.");
      return res.status(400).json({ success: false, data: "Incorrect input: only float values are acceptable."});
    }

    var left = parseFloat(req.params.left);
    var bottom = parseFloat(req.params.bottom);
    var right = parseFloat(req.params.right);
    var top = parseFloat(req.params.top);

    if(left >= right) {
      done();
      console.log("Incorrect input: western longitude value should be less than eastern longitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: western longitude value should be less than eastern longitude value."});
    }

    if(bottom >= top) {
      done();
      console.log("Incorrect input: southern latitude value should be less than northern latitude value.");
      return res.status(400).json({ success: false, data: "Incorrect input: southern latitude value should be less than northern latitude value."});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              zone_type as roadCondition
          ) d
        )) as properties
      FROM gfs_road_conditions
      WHERE forecast_time = ($1) AND geom && 
      ST_MakeEnvelope($2, $3, $4, $5, 4326)
    `,[forecast_time_int,left, bottom, right, top]);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/windspeed\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'windspeed'
      ORDER BY risk_level
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});


app.get('/event-windspeed\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM event_polygon_contours
      WHERE variable_name = 'windspeed'
      ORDER BY risk_level
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

app.get('/posh\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'posh'
      ORDER BY risk_level
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

app.get('/lightning\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM gfs_contours
      WHERE variable_name = 'lightning'
      ORDER BY risk_level
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

app.get('/reflectivity\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              risk_level as riskLevel,
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM polygon_contours
      WHERE variable_name = 'seamless'
      ORDER BY minimum_value
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

app.get('/vehicles\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      WITH qpe as (
        SELECT
          case risk_level
            when 1 then 'low'
            when 2 then 'medium'
            when 3 then 'high'
          end as alert,
          ARRAY[minimum_value, maximum_value] as thresholds,
          geom
        FROM
        polygon_contours
        WHERE variable_name = 'qpe'
      ), wind as (
        SELECT
          ARRAY[minimum_value, maximum_value] as thresholds,
          case risk_level
            when 1 then 'low'
            when 2 then 'medium'
            when 3 then 'high'
          end as alert,
          geom
        FROM
        polygon_contours
        WHERE variable_name = 'windspeed'
      ), vis as (
        SELECT
          ARRAY[minimum_value, maximum_value] as thresholds,
          case risk_level
            when 1 then 'low'
            when 2 then 'medium'
            when 3 then 'high'
          end as alert,
          geom
        FROM
        polygon_contours
        WHERE variable_name = 'visibility'
      ), posh as (
        SELECT
          ARRAY[minimum_value, maximum_value] as thresholds,
          case risk_level
            when 1 then 'low'
            when 2 then 'medium'
            when 3 then 'high'
          end as alert,
          geom
        FROM
        polygon_contours
        WHERE variable_name = 'posh'
      ), lit as (
        SELECT
          ARRAY[minimum_value, maximum_value] as thresholds,
          case risk_level
            when 1 then 'low'
            when 2 then 'medium'
            when 3 then 'high'
          end as alert,
          geom
        FROM
        polygon_contours
        WHERE variable_name = 'lightning'
      )
      select
        'Feature' as type,
        ST_AsGeoJSON(v.geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              v.time,
              row_to_json((
                SELECT f FROM (
                  SELECT
                    'wtx-' || v.vehicle_number::text as device_number,
                    v.id as id,
                    'wtx' as data_source
                ) f
              )) as metadata,
              qpe.alert as qpe_alert,
              qpe.thresholds as qpe_alert_values,
              wind.alert as windspeed_alert,
              wind.thresholds as windspeed_alert_values,
              vis.alert as visibility_alert,
              vis.thresholds as visibility_alert_values,
              posh.alert as posh_alert,
              posh.thresholds as posh_alert_values,
              lit.alert as lightning_alert,
              lit.thresholds as lightning_alert_values
          ) d
        )) as properties
      from (
        select
          id,
          time,
          vehicle_number,
          geom
        from unique_wtx_vehicles
      ) as v
      left outer join qpe
        on ST_Intersects(qpe.geom, v.geom)
      left outer join wind
        on ST_Intersects(wind.geom, v.geom)
      left outer join vis
        on ST_Intersects(vis.geom, v.geom)
      left outer join posh
        on ST_Intersects(posh.geom, v.geom)
      left outer join lit
        on ST_Intersects(lit.geom, v.geom)
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      results = fuzionUtilities.addColourFromAlerts(results);

      var geoJSON = fuzionUtilities.createGeoJSON(results);
      var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});

      return res.json(topology);
    });
  });

});

/* Cameras will stay in mongo, seb is migrating the data to postgres so we can kill mongo */

app.get('/traffic-cameras', function (req, res) {

  var agg = TrafficCameras.aggregate([
    { $project:
      {
        _id: false,
        type: { $literal: "Feature" },
        geometry: "$location",
        properties: {
          name: "$name",
          cameraID: "$cameraID",
          regionID: "$regionID",
          closestLane: "$closestLane",
          markerPost: "$markerPost",
          detectedOutOfService: "$detectedOutOfService",
          streamWebAddress: "$streamWebAddress"
        }
      }
    }
  ]);

  agg.exec(function(err, trafficCameras) {
    if(err)
      console.log(err);

    var geoJSON = fuzionUtilities.createGeoJSON(trafficCameras);
    var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});

    res.json(topology);
  });

});

app.get('/forecast/:api_key/:lat,:lng', function(req, res) {
  var forecastUrl = 'https://api.forecast.io/forecast/' + req.params['api_key'] + '/' + req.params['lat'] + ',' + req.params['lng'];
  request(forecastUrl).pipe(res);
});


app.get('/forecast_point/:lat,:lng', function(req, res) {
  var forecastUrl = 'https://api.forecast.io/forecast/b9d234279c0f95ca8cdebdc29d6e3c62/' + req.params['lat'] + ',' + req.params['lng'];
  request(forecastUrl).pipe(res);
});


/* Post endpoint for getting vehicle warnings */

app.post('/warnings', function(req, res) {
  
  var vehicles = req.body;

  if (!vehicles)
    res.json('{}')

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    console.log(JSON.stringify(vehicles));

    var query = client.query(`
      with dataset as (
        select
          id,
          lng,
          lat
        from json_to_recordset('${ JSON.stringify(vehicles)}') as x(id text, lng float, lat float)
      ),
      locs as (
        select id, st_setsrid(st_makepoint(lng, lat), 4326) as geom from dataset
      ),
      qpe as (
      SELECT
        case risk_level
          when 1 then 'low'
          when 2 then 'medium'
          when 3 then 'high'
        end as alert,
        ARRAY[minimum_value, maximum_value] as thresholds,
        geom
      FROM
      polygon_contours
      WHERE variable_name = 'qpe'
      ), wind as (
      SELECT
        ARRAY[minimum_value, maximum_value] as thresholds,
        case risk_level
          when 1 then 'low'
          when 2 then 'medium'
          when 3 then 'high'
        end as alert,
        geom
      FROM
      polygon_contours
      WHERE variable_name = 'windspeed'
      ), vis as (
      SELECT
        ARRAY[minimum_value, maximum_value] as thresholds,
        case risk_level
          when 1 then 'low'
          when 2 then 'medium'
          when 3 then 'high'
        end as alert,
        geom
      FROM
      polygon_contours
      WHERE variable_name = 'visibility'
      ), posh as (
      SELECT
        ARRAY[minimum_value, maximum_value] as thresholds,
        case risk_level
          when 1 then 'low'
          when 2 then 'medium'
          when 3 then 'high'
        end as alert,
        geom
      FROM
      polygon_contours
      WHERE variable_name = 'posh'
      ), lit as (
      SELECT
        ARRAY[minimum_value, maximum_value] as thresholds,
        case risk_level
          when 1 then 'low'
          when 2 then 'medium'
          when 3 then 'high'
        end as alert,
        geom
      FROM
      polygon_contours
      WHERE variable_name = 'lightning'
      )

      select
        l.id,
        row_to_json((
          SELECT d FROM (
            SELECT
              qpe.alert as qpe_alert,
              qpe.thresholds as qpe_alert_values,
              wind.alert as windspeed_alert,
              wind.thresholds as windspeed_alert_values,
              vis.alert as visibility_alert,
              vis.thresholds as visibility_alert_values,
              posh.alert as posh_alert,
              posh.thresholds as posh_alert_values,
              lit.alert as lightning_alert,
              lit.thresholds as lightning_alert_values
          ) d
        )) as properties
      from locs l
      left outer join qpe
        on ST_INTERSECTS(qpe.geom, l.geom)
      left outer join wind
        on ST_INTERSECTS(wind.geom, l.geom)
      left outer join vis
        on ST_INTERSECTS(vis.geom, l.geom)
      left outer join posh
        on ST_INTERSECTS(posh.geom, l.geom)
      left outer join lit
        ON ST_INTERSECTS(lit.geom, l.geom)
    `, 
    function(err, result) {
      done();

      if(err)
        return res.status(500).json({ success: false, data: err});

      console.log(result);
      
      var results = fuzionUtilities.addColourFromAlerts(result.rows);
      return res.json(results);
    });
  });

});

app.get('/road-temp\.:ext?', fuzionUtilities.defaultParamMiddleware, function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
      SELECT 
        'Feature' as type,
        ST_AsGeoJSON(geom)::json as geometry,
        row_to_json((
          SELECT d FROM (
            SELECT
              colour,
              ARRAY[minimum_value, maximum_value] as thresholdValues,
              variable_name as variableName
          ) d
        )) as properties
      FROM road_temperature_contours
      WHERE variable_name = 'road_temperatures'
      ORDER BY minimum_value
    `);

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);

      if (req.params.ext == "geojson") {
        return res.json(geoJSON);
      } else {
        var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});
        return res.json(topology);
      }

    });
  });

});

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
