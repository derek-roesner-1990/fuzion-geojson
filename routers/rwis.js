var fuzionUtilities = require('../lib/utilities');

var express = require('express') ;
var app = express.Router() ;
var math = require('mathjs');

/******************************************************************************
                               Config Setup
******************************************************************************/

var env = process.env.NODE_ENV || 'development' ;
var config = require('../config.js')[env] ;

/******************************************************************************
                              DB Setup
******************************************************************************/

var pg = require('pg');
var connectionString = config.pg_db;

/******************************************************************************
                                Routes
******************************************************************************/

app.get('/', function (req, res) {
  res.send("<p>RWIS QC Data Here</p>");
});

app.get('/temp', function (req, res) {

  var results = [];

  pg.connect(connectionString, function(err, client, done) {

    if(err) {
      done();
      console.log(err);
      return res.status(500).json({ success: false, data: err});
    }

    var query = client.query(`
    select distinct on (stationName)
      'Feature' as type,
      ST_AsGeoJSON(geom)::json as geometry,
      row_to_json((
        SELECT d FROM (
          SELECT
            stationName,
            time, 
            temperature
        ) d
      )) as properties
    from rwis_observations 
    order by stationName, time DESC;
    `);

    query.on('row', function(row) {
      row.properties.colour = fuzionUtilities.colourFromTemp(row.properties.temperature)
      results.push(row);
    });

    query.on('end', function() {
      done();

      var geoJSON = fuzionUtilities.createGeoJSON(results);
      //var topology = topojson.topology({collection: geoJSON},{"property-transform":function(object){return object.properties;}});

      return res.json(geoJSON);
    });
  });
});

/**********************************************************************************************
    RWIS data is not up to date, commenting out for now... will need to write query in PG
***********************************************************************************************

app.get('/road_temp', function (req, res) {

  var results = [];

  NetCDFObs.distinct('metadata.deviceNumber', { 'metadata.dataSource':'mesonet', 'observation.road.temperatures': { $elemMatch: { $ne: NaN } } }, function(err, distinctRWIS) {
    if(err)
      console.log(err);

    //Skinny the results to be able to debug quicker...
    //distinctRWIS = distinctRWIS.slice(0,17);

    var rwis_promises = distinctRWIS.map(function(stationNumber) {
      return new Promise(function(resolve, reject) {
        var agg = NetCDFObs.aggregate([
          { $match: { 'metadata.deviceNumber': stationNumber, 'observation.road.temperatures': { $elemMatch: { $ne: NaN } } } },
          { $sort: { time: -1 } },
            { $limit: 1 },
            { $project:
              {
                _id: false,
                type: { $literal: "Feature" },
                geometry: {
                  type: { $literal: "MultiLineString" }
                },
                properties:
                {
                  time: "$time",
                  position: "$position",
                  roadtemperatures: "$observation.road.temperatures",
                  metadata: "$metadata"
                }
              }
            }
        ]);
        agg.exec(function (rwis_err, stations) {
          if(rwis_err)
            return reject(rwis_err);

          var station_promises = stations.map(function(station) {
            return new Promise(function(resolve, reject) {
              var temp = station.properties.roadtemperatures;
              var roadtemperatures = [];

              for(var j = 0; j < temp.length; j++){
                if (!isNaN(temp[j]))
                  roadtemperatures.push(temp[j]);
              }

              var roadtemperature = math.mean(roadtemperatures);

              station.properties.roadTemperature = roadtemperature;

              var location = station.properties.position.coordinates;

              var rsAgg = RoadSegments.aggregate([
                {
                  $geoNear: {
                    near: { type: "Point", coordinates: location },
                    distanceField: "dist.calculated",
                    maxDistance: 5000,
                    spherical: true
                  }
                },
                {
                  $group: {
                    _id: null,
                    coordinates: { $push: "$coordinates"}
                  }
                }
              ]);

              rsAgg.exec(function(rs_err, rss){
                if(rs_err)
                  return reject(rs_err);

                if(rss.length === 0){
                  // If there are no road segments returned from the query,
                  // change the geometry to a LineString with the same start
                  // and end point which will render a point.
                  station.geometry.type = "LineString";
                  station.geometry.coordinates = [station.properties.position.coordinates, station.properties.position.coordinates];
                }
                else if(rss.length != 1)
                  console.log("WARNING: RoadSegmentAgg Count was not expexted: " + rss.length + ", station: " + station.properties.metadata.deviceNumber);
                else {
                  var rs = rss[0];
                  station.geometry.coordinates = rs.coordinates;
                }

                resolve();

              });
            });
          });

          Promise.all(station_promises)
          .then(function(){
            results = results.concat(stations);
            resolve();
          })
          .catch(console.error);

        });
      });
    });

    Promise.all(rwis_promises)
    .then(function() {
      console.log("RWIS Road Temperature Observations:" + results.length);
      results = fuzionUtilities.addRoadTempColour(results);
      var geoJSON = fuzionUtilities.createGeoJSON(results);
      res.json(geoJSON);
    })
    .catch(console.error);

  });
});

app.get('/road_state', function (req, res) {

  var results = [];

  NetCDFObs.distinct('metadata.deviceNumber', { 'metadata.dataSource':'mesonet', 'observation.road.states': { $elemMatch: { $ne: NaN } } }, function(err, distinctRWIS) {
    if(err)
      console.log(err);

    //Skinny the results to be able to debug quicker...
    //distinctRWIS = distinctRWIS.slice(0,17);

    var rwis_promises = distinctRWIS.map(function(stationNumber) {
      return new Promise(function(resolve, reject) {
        var agg = NetCDFObs.aggregate([
          { $match: { 'metadata.deviceNumber': stationNumber, 'observation.road.states': { $elemMatch: { $ne: NaN } } } },
          { $sort: { time: -1 } },
            { $limit: 1 },
            { $project:
              {
                _id: false,
                type: { $literal: "Feature" },
                geometry: {
                  type: { $literal: "MultiLineString" }
                },
                properties:
                {
                  time: "$time",
                  position: "$position",
                  roadstates: "$observation.road.states",
                  metadata: "$metadata"
                }
              }
            }
        ]);
        agg.exec(function (rwis_err, stations) {
          if(rwis_err)
            return reject(rwis_err);

          var station_promises = stations.map(function(station) {
            return new Promise(function(resolve, reject) {
              var temp = station.properties.roadstates;
              var roadstates = [];

              for(var j = 0; j < temp.length; j++){
                if (!isNaN(temp[j]))
                  roadstates.push(temp[j]);
              }

              var roadstate = math.mode(roadstates)[0];

              station.properties.roadstate = roadstate;
              station.properties.roadstatedesc = fuzionUtilities.getRoadStateDesc(roadstate);
              station.properties.metadata.colour = fuzionUtilities.getRoadStateColour(roadstate);

              var location = station.properties.position.coordinates;

              var rsAgg = RoadSegments.aggregate([
                {
                  $geoNear: {
                    near: { type: "Point", coordinates: location },
                    distanceField: "dist.calculated",
                    maxDistance: 5000,
                    spherical: true
                  }
                },
                {
                  $group: {
                    _id: null,
                    coordinates: { $push: "$coordinates"}
                  }
                }
              ]);

              rsAgg.exec(function(rs_err, rss){
                if(rs_err)
                  return reject(rs_err);

                if(rss.length === 0){
                  // If there are no road segments returned from the query,
                  // change the geometry to a LineString with the same start
                  // and end point which will render a point.
                  station.geometry.type = "LineString";
                  station.geometry.coordinates = [station.properties.position.coordinates, station.properties.position.coordinates];
                }
                else if(rss.length != 1)
                  console.log("WARNING: RoadSegmentAgg Count was not expexted: " + rss.length + ", station: " + station.properties.metadata.deviceNumber);
                else {
                  var rs = rss[0];
                  station.geometry.coordinates = rs.coordinates;
                }

                resolve();

              });
            });
          });

          Promise.all(station_promises)
          .then(function(){
            results = results.concat(stations);
            resolve();
          })
          .catch(console.error);

        });
      });
    });

    Promise.all(rwis_promises)
    .then(function() {
      console.log("RWIS Road State Observations:" + results.length);
      var geoJSON = fuzionUtilities.createGeoJSON(results);
      res.json(geoJSON);
    })
    .catch(console.error);

  });
});

*/

/******************************************************************************
                                Export
******************************************************************************/

module.exports = app ;
