# Fuzion GeoJSON

This project is an example of what I maintained. Not written by me but I did make some additions and fixes to the codebase.


## Installation

1.  Install Node Version Manager

    On OS X, `brew install nvm` and add

        # Node Version Manager (nvm)
        export NVM_DIR=~/.nvm
        . $(brew --prefix nvm)/nvm.sh

    to your `~/.bash_profile` (or your desired shell)

2.  Install Node version 5.6

        nvm install 5.6

3.  Set the node version

        nvm use 5.6

4.  Install dependencies

        npm install

5.  Test

        npm test

6.  Run the server

        npm start

7.  Access in your browser

        http://localhost:4050


## Development

For now, the application structure is as follows:

- config.js

  Contains configuration info (e.g. DB connection string)

- lib

  Contains miscelaneous libraries (e.g. utilities)

- models

  Contains Mongoose Models

- routers

  Contains routers for each data source.  Core service logic happens here for now, though it should be moved into controllers.

- schemas

  Contains Mongoose Schemas (e.g. shared NetCDF/WTX schema)

- server.js

  The main service entry point

- test

  Tests (using Mocha and Chai/Chai-HTTP)


