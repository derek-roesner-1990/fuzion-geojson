var mongoose = require('mongoose') ;
var Schema = mongoose.Schema ;

module.exports = new Schema({ 
  _id : Schema.Types.ObjectId, 
  observation_atmosphere_temperature : Number, 
  observation_atmosphere_wind_gust : Number, 
  allScores : { 
    observation_atmosphere_temperature : [Number], 
    observation_atmosphere_wind_gust : [Number], 
    observation_atmosphere_wind_speed : [Number], 
    observation_atmosphere_relativeHumidity : [Number], 
    observation_atmosphere_visibility : [Number], 
    observation_road_temperatures : [Number], 
    observation_atmosphere_wind_direction : [Number], 
    observation_atmosphere_dewPoint : [Number], 
    observation_road_states : [Number] 
  }, 
  observation_atmosphere_wind_speed : Number, 
  observation_atmosphere_relativeHumidity : Number, 
  overallScore : Number, 
  deviceNumber : String, 
  observation_road_temperatures : Number, 
  observation_atmosphere_visibility : Number, 
  observation_atmosphere_wind_direction : Number, 
  startTime : Number, 
  endTime : Number, 
  observation_atmosphere_dewPoint : Number, 
  observation_road_states : Number
});

