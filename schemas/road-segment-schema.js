var mongoose = require('mongoose') ;
var Schema = mongoose.Schema ;

module.exports = new Schema({
  _id: Schema.Types.ObjectId,
  type: String,
  coordinates: [],
  properties: {
    scalerank: Number,
    state: String,
    country: String,
    midpoint: {
      type: String,
      coordinates: []
    }
  }
});