var mongoose = require('mongoose') ;
var Schema = mongoose.Schema ;

module.exports = new Schema({
  _id : Schema.Types.ObjectId,
  position : {
      elevation : Number,
      coordinates : [Number]
  },
  metadata : {
      deviceNumber : String,
      id : Number,
      dataSource : String
  },
  qpeAlertValues: [Number],
  qpeAlert: [Number],
  visibilityAlertValues: [Number],
  visibilityAlert: [Number],
  windspeedAlertValues: [Number],
  windspeedAlert: [Number],
  poshAlertValues: [Number],
  poshAlert: [Number],
  lightningAlertValues: [Number],
  lightningAlert: [Number],
  observation : {
      precipitation : {
          intensity : Number
      },
      atmosphere : {
          ozone : Number,
          stationPressure : Number,
          temperature : Number,
          relativeHumidity : Number
      },
      radiation : {
          lightLevel : Number
      },
      road : {
          temperatures : [Number]
      },
      velocity : {
          speed : Number
      }
  },
  time : Number
});