var mongoose = require('mongoose') ;
var Schema = mongoose.Schema ;

module.exports = new Schema({
  _id : Schema.Types.ObjectId,
  cameraID: Number,
  name: String,
  location : {
    type : String,
    coordinates : [Number]
  },
  closestLane: String,
  updateFrequency: String,
  detectedOutOfService: Boolean,
  markerPost: Number,
  streamWebAddress: String,
  regionID: Number
});