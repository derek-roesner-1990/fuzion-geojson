var mongoose = require('mongoose') ;
var Schema = mongoose.Schema ;

module.exports = new Schema({
  position: {
    elevation: Number,
    coordinates: [Number]
  },
  time: Number,
  observation: {
    atmosphere: {
      temperature: Number
    }
  },
  metadata: {
    deviceNumber: String
  }
});
