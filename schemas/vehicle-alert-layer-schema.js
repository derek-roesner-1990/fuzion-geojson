var mongoose = require('mongoose') ;
var Schema = mongoose.Schema ;

module.exports = new Schema({
  _id : Schema.Types.ObjectId,
  geometry : {
    type : String,
    coordinates : []
  },
  properties : {
    thresholdValues : [Number],
    variableName : String,
    colour : String
  }
});