var Colour = require("color");
var math = require("mathjs");

module.exports = {

  createGeoJSON: function (docs) {
    var results = new Object();
    results.type = "FeatureCollection";
    results.features = docs;

    return results;
  },
  addColourFromAlerts: function(data){
    for(var i = 0; i < data.length; i++){
      var datum = data[i];

      var main_colour;

      if (datum.properties.qpe_alert === 'high' || datum.properties.visibility_alert === 'high' || datum.properties.windspeed_alert === 'high' || datum.properties.posh_alert === 'high' || datum.properties.lightning_alert === 'high')
        main_colour = "red";
      else if (datum.properties.qpe_alert === 'medium' || datum.properties.visibility_alert === 'medium' || datum.properties.windspeed_alert === 'medium' || datum.properties.posh_alert === 'medium' || datum.properties.lightning_alert === 'medium')
        main_colour = "orange";
      else if (datum.properties.qpe_alert === 'low' || datum.properties.visibility_alert === 'low' || datum.properties.windspeed_alert === 'low' || datum.properties.posh_alert === 'low' || datum.properties.lightning_alert === 'low')
        main_colour = "yellow";
      else
        main_colour = "green";

      var qpe_alert_colour;

      switch(datum.properties.qpe_alert) {
        case "high":
          qpe_alert_colour = "red";
          break;
        case "medium":
          qpe_alert_colour = "orange";
          break;
        case "low":
          qpe_alert_colour = "yellow";
          break;
        default:
          qpe_alert_colour = "green";
      }

      var windspeed_alert_colour;

      switch(datum.properties.windspeed_alert) {
        case "high":
          windspeed_alert_colour = "red";
          break;
        case "medium":
          windspeed_alert_colour = "orange";
          break;
        case "low":
          windspeed_alert_colour = "yellow";
          break;
        default:
          windspeed_alert_colour = "green";
      }

      var visibility_alert_colour;

      switch(datum.properties.visibility_alert) {
        case "high":
          visibility_alert_colour = "red";
          break;
        case "medium":
          visibility_alert_colour = "orange";
          break;
        case "low":
          visibility_alert_colour = "yellow";
          break;
        default:
          visibility_alert_colour = "green";
      }

      var posh_alert_colour;

      switch(datum.properties.posh_alert) {
        case "high":
          posh_alert_colour = "red";
          break;
        case "medium":
          posh_alert_colour = "orange";
          break;
        case "low":
          posh_alert_colour = "yellow";
          break;
        default:
          posh_alert_colour = "green";
      }

      var lightning_alert_colour;

      switch(datum.properties.lightning_alert) {
        case "high":
          lightning_alert_colour = "red";
          break;
        case "medium":
          lightning_alert_colour = "orange";
          break;
        case "low":
          lightning_alert_colour = "yellow";
          break;
        default:
          lightning_alert_colour = "green";
      }

      datum.properties.colours = {
        main_colour: main_colour,
        qpe_alert_colour: qpe_alert_colour,
        windspeed_alert_colour: windspeed_alert_colour,
        visibility_alert_colour: visibility_alert_colour,
        posh_alert_colour: posh_alert_colour,
        lightning_alert_colour: lightning_alert_colour
      }
    }

    return data;
  },
  colourFromTemp: function (temp) {
    var r;
    var g;
    var b;
    var delta;
    var tempHigh = 308.15;
    var tempMid = 288.15;
    var tempLow = 273.15;
    var tempExtraLow = 243.15;

    var down = function(high, low, temp, value) {
      var delta = high - low;
      return Math.floor((1.0 - ((temp - low) / (delta))) * value);
    };

    var up = function(high, low, temp, value) {
      var delta = high - low;
      return Math.floor(((temp - low) / (delta)) * value);
    };

    if (temp >= tempHigh) {
      r = 255;
      g = 128;
      b = 0;
    } else if (temp >= tempMid) {
      r = 255;
      g = up(tempHigh, tempMid, temp, 128);
      b = 0;
    } else if (temp >= tempLow) {
      r = up(tempMid, tempLow, temp, 255);
      g = 0;
      b = down(tempMid, tempLow, temp, 255);
    } else if (temp >= tempExtraLow) {
      r = down(tempLow, tempExtraLow, temp, 255);
      g = down(tempLow, tempExtraLow, temp, 255);
      b = 255;
    } else {
      r = 255;
      g = 255;
      b = 255;
    }

    var colour = Colour().rgb(r, g, b);
    return colour.hexString();
  },
  addTempColour: function (docs) {
    for(var i = 0; i < docs.length; i++){
      doc = docs[i];
      var temp = doc.properties.observation.atmosphere.temperature;
      var tempQCD = doc.properties.observation.atmosphere.temperatureQCD;
      tempQCD = isNaN(tempQCD) ? 0 : tempQCD;
      var tempFixed = temp + tempQCD;
      doc.properties.observation.atmosphere.temperatureFixed = tempFixed;
      doc.properties.metadata.colour = this.colourFromTemp(tempFixed);
    }

    return docs;
  },
  addRoadTempColour: function (docs) {
    for(var i = 0; i < docs.length; i++){
      doc = docs[i];
      var temp = doc.properties.roadTemperature ;
      doc.properties.metadata.colour = this.colourFromTemp(temp);
    }

    return docs;
  },
  getRoadStateDesc: function (state) {
    switch (state) {
      case 0:
        return "No Report";
      case 1:
        return "Dry";
      case 2:
        return "Moist";
      case 3:
        return "Moist with chemical";
      case 4:
        return "Wet";
      case 5:
        return "Wet with chemical";
      case 6:
        return "Ice";
      case 7:
        return "Frost";
      case 8:
        return "Snow";
      case 9:
        return "Snow/Ice Watch";
      case 10:
        return "Snow/Ice Warning";
      case 15:
        return "Dew";
      case 17:
        return "Other";
      default:
        return "UNKNOWN!";
    }
  },
  getRoadStateColour: function (state) {
    switch (state) {
      case 0:
        return "#000000"; // No Report
      case 1:
        return "#269926"; // Dry
      case 2:
        return "#9DB82E"; // Moist
      case 3:
        return "#9DB82E"; // Moist with chemical
      case 4:
        return "#FFC500"; // Wet
      case 5:
        return "#FFC500"; // Wet With chemical
      case 6:
        return "#FF8888"; // Ice
      case 7:
        return "#00BBFF"; // Frost
      case 8:
        return "#3CA0D0"; // Snow
      case 9:
        return "#3CA0D0"; // Snow/Ice Watch
      case 10:
        return "#FF8888"; // Snow/Ice Warning
      case 15:
        return "#000000"; // Dew
      case 17:
        return "#000000"; // Other
      default:
        return "#000000"; // UNKNOWN!
    }
  },
  getQCColour: function (qcValue) {
    var adjustedQCValue = math.round(qcValue, 1);

    if(adjustedQCValue >= 0.9){
      return "#000000"
    } 
    else
    {
      var r = 255;
      var g = 255 * adjustedQCValue;
      var b = 0;

      var colour = Colour().rgb(r, g, b);
      return colour.hexString();
    }
  },
  defaultParamMiddleware: function(req, res, next) {
    if (!req.params.ext) {
        req.params.ext = 'topojson';
    }

    next();
  }
};
